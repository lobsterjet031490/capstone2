const User = require('../models/User.js');
const Product = require('../models/Product.js');
const bcrypt = require('bcrypt');
const auth = require('../auth.js');


module.exports.registerUser = (request_body) => {
	let new_user = new User({
		firstName: request_body.firstName,
		lastName: request_body.lastName,
		email: request_body.email,
		password: bcrypt.hashSync(request_body.password, 10)
	});

	return new_user.save().then((register_user, error) => {
		if(error){
			return {
				message: error.message
			}
		}

		return {
			message: 'Succesfully registered a user!'
		};
	}).catch(error => console.log(error));
}

module.exports.loginUser = (request, response) =>{
	return User.findOne({email: request.body.email}).then((result) =>{
		if(result == null){
			return response.send({
			})
		}
		const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);

		if(isPasswordCorrect){
			return response.send({
				accessToken: auth.createAccessToken(result),
				userId: result._id
			});
		} else {
			return response.send({
				message: 'Your email or password is incorrect'
			})
		}
	}).catch(error => response.send(error));
}

module.exports.userDetails = (request_body) => {
	return User.findOne({_id: request_body.id}).then((user, error) =>{
		if(error){
			return {
				message: error.message
			}
		}
		user.password = "";
		return user;
	}).catch(error => console.log(error));
}

module.exports.createOrder = async (request, response) => {

	if(request.user.isAdmin){
		return response.send('Action Forbidden');
	}

	let isUserUpdated = await User.findById(request.user.id).then(user => {
		let new_order = {
			productId: request.body.productId,
			productName: request.body.productName,
			quantity: request.body.quantity
		}
		const existingProduct = user.orderedProducts.find(o => o.productId === new_order.productId)
		if(existingProduct){
			user.orderedProducts = user.orderedProducts.map((op) => {
  				return (op.productId === existingProduct.productId) ? {
   						 ...op,
    					quantity: op.quantity + new_order.quantity
  				} : op
			})
		}else{
			user.orderedProducts.push(new_order)
		}
		
		return user.save().then(updated_user => true).catch(error => error.message)
	})

	if(isUserUpdated !== true){
		return response.send({ message: isUserUpdated });
	}

	if(isUserUpdated){
		return response.send({ message: 'Purchase successfully!' });
	}
}