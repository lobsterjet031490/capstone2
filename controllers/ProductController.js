const Product = require('../models/Product.js');

module.exports.addProduct = (request, response) => {
	let new_product = new Product({
		name: request.body.name,
		description: request.body.description,
		price: request.body.price
	});

	return new_product.save().then((saved_product, error) => {
		if(error){
			return response.send(false);
		}
		return response.send(true);
	}).catch(error => console.log(error));
}

module.exports.retrieveAllProduct = (request, response) => {
	return Product.find({}).then(result => {
		return response.send(result);
	})
}

module.exports.activeAllProduct = (request, response) => {
	return Product.find({isActive: true}).then(result => {
		return response.send(result);
	})
}

module.exports.singleProduct = (request_body) => {
	return Product.findById(request_body).then(result => {
		return result;
	})
}

module.exports.updateProduct = (request, response) => {
	let updated_product_information = {
		name: request.body.name,
		description: request.body.description,
		price: request.body.price
	};
	return Product.findByIdAndUpdate(request.params.id, updated_product_information).then((result, error) => {
		if(error){
			return response.send({
				message: error.message
			})
		}
		return response.send({
			message: 'Product successfully updated!'
		})
	})
}

module.exports.archiveProduct = (request, response) => {
	return Product.findByIdAndUpdate(request.params.id, {isActive: false}).then((result, error) => {
		if(error){
			return response.send(false)
		}
		return response.send(true)
	})
}

module.exports.activateProduct = (request, response) => {
	return Product.findByIdAndUpdate(request.params.id, {isActive: true}).then((result, error) => {
		if(error){
			return response.send(false)
		}
		return response.send({
			message: 'Product Activated'
		})
	})
}