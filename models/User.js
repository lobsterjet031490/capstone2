const mongoose = require('mongoose');


const user_schema = new mongoose.Schema({
    firstName: {
        type: String,
        required: [true, "Firstname is required"]
    },
    lastName: {
        type: String,
        required: [true, "Lastname is required"]
    },
    email: {
        type: String,
        required: [true, "Email is required"]
    },
   password: {
        type: String,
        required: [true, "Password is required"]
    },
    isAdmin: {
        type: Boolean,
        default: true
    },
    orderedProducts: [{
            productId: {
                type: String,
                required: [true, "ProductId is required"]
            },
            productName: {
                type: String,
                required: [true, "ProductName is required"]
            },
            quantity: {
                type: Number,
                required: [true, "Quantity is required"]
            },
    }]
})

module.exports = mongoose.model("User", user_schema);