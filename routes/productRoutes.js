const express = require('express');
const router = express.Router();
const auth = require('../auth.js');
const ProductController = require('../controllers/ProductController.js');

router.post('/', auth.verify, auth.verifyAdmin, (request, response) =>{
	ProductController.addProduct(request, response)
});

router.get('/all', (request, response) => {
	ProductController.retrieveAllProduct(request, response)
});

router.get('/', (request, response) => {
	ProductController.activeAllProduct(request, response)
});

router.get('/:id', (request, response) => {
	ProductController.singleProduct(request.params.id).then((result) => {
		response.send(result)
	})
});

router.put('/:id', auth.verify, auth.verifyAdmin, (request, response) => {
	ProductController.updateProduct(request, response)
});

router.put('/:id/archive', auth.verify, auth.verifyAdmin, (request, response) => {
	ProductController.archiveProduct(request, response)
});

router.put('/:id/activate', auth.verify, auth.verifyAdmin, (request, response) => {
	ProductController.activateProduct(request, response)
});


module.exports = router;